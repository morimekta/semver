PACKAGE=semver-py
VERSION=$(shell cat ./src/bin/semver.py | grep '__version__ = ' | sed -e "s/.*= //" -e "s:'::g")
TARGET=$(HOME)/.local

OUT=$(patsubst out/bin/%.py, out/bin/%, $(patsubst src/%, out/%, $(wildcard src/*/*.py)))
PKG=$(PACKAGE)-$(VERSION).tar.gz $(PACKAGE)-$(VERSION)_all.deb $(PACKAGE)-$(VERSION)-1.noarch.rpm

FILES=$(patsubst out/%, %, $(OUT))

TESTS=$(wildcard *_test.bash)

## -- Specific Rules

default:
	@echo 'make clean'
	@echo 'make package'
	@echo 'make install'
	@echo 'make upload'
	@echo 'make test'

clean:
	rm -rf out/ out-*/ tmp/ $(PACKAGE)-*.tar.gz $(PACKAGE)-*.rpm $(PACKAGE)-*.deb

package: $(PKG)
	@echo
	@echo 'DONE: Packaging'
	@for f in $(PKG); do echo ' --' $$f; done

install: $(OUT)
	@mkdir -p $(TARGET)/
	cp -R out/* $(TARGET)/

upload: package homebrew/semver-py.rb
	@$(eval DL_MORIMEKTA_NET:=$(shell gt-repo.sh -d dl.morimekta.net))
	@$(eval HOMEBREW_TOOLS:=$(shell   gt-repo.sh -d homebrew-tools))
	@$(eval SHA256SUM:=$(shell cat $(PACKAGE)-$(VERSION).tar.gz | sha256sum | sed 's/ .*//'))
	cp $(PACKAGE)-$(VERSION).tar.gz       $(DL_MORIMEKTA_NET)/dl/archive
	cp $(PACKAGE)-$(VERSION)_all.deb      $(DL_MORIMEKTA_NET)/dl/deb
	cp $(PACKAGE)-$(VERSION)-1.noarch.rpm $(DL_MORIMEKTA_NET)/dl/rpm
	cat homebrew/semver-py.rb | sed -e 's/__VERSION__/$(VERSION)/' -e 's/__SHA256SUM__/$(SHA256SUM)/' \
        > $(HOMEBREW_TOOLS)/Formula/semver-py.rb

test: $(TESTS)
	shunit2 $<

## -- General Rules

out/bin/%: src/bin/%.py
	@mkdir -p $(dir $@)
	cp $< $@

out/%.py: src/%.py
	@mkdir -p $(dir $@)
	cp $< $@

## -- .tar.gz

$(PACKAGE)-$(VERSION).tar.gz: $(OUT)
	@rm -f $@
	tar zcvf $@ --sort name -C out $(patsubst out/%, %, $(OUT))

## -- .deb

DEBIAN=$(patsubst debian/%, out-debian/DEBIAN/%, $(wildcard debian/*))
DEBIAN_OUT=$(patsubst out/%, out-debian/usr/local/%, $(OUT))

out-debian/DEBIAN/%: debian/% $(OUT)
	@mkdir -p $(dir $@)
	cp $< $@
	@$(eval SIZE:=$(shell cat $(OUT) | wc -c))
	sed -i 's/__VERSION__/$(VERSION)/' $@
	sed -i 's/__SIZE__/$(SIZE)/' $@

out-debian/usr/local/%: out/%
	@mkdir -p $(dir $@)
	cp $< $@

$(PACKAGE)-$(VERSION)_all.deb: $(DEBIAN) $(DEBIAN_OUT)
	fakeroot dpkg-deb -b out-debian $@

## -- .rpm

RPM_SPEC=out-rpm/rpmbuild/SPECS/semver-py.spec
RPM_OUT=$(patsubst out/%, out-rpm/out/usr/local/%, $(OUT))

out-rpm/out/usr/local/%: out/%
	@mkdir -p $(dir $@)
	cp $< $@

out-rpm/rpmbuild/SPECS/semver-py.spec: rpmbuild/semver-py.spec
	@mkdir -p $(dir $@)
	cp $< $@
	sed -ui 's/__VERSION__/$(VERSION)/' $@
	sed -ui 's:__ROOT__:$(PWD):g' $@

$(PACKAGE)-$(VERSION)-1.noarch.rpm: $(PACKAGE)-$(VERSION).tar.gz $(RPM_SPEC) $(RPM_OUT)
	HOME=$(PWD)/out-rpm rpmbuild --build-in-place --buildroot $(PWD)/out-rpm/out --target noarch -bb out-rpm/rpmbuild/SPECS/semver-py.spec
	@mv out-rpm/rpmbuild/RPMS/noarch/$(PACKAGE)-$(VERSION)-1.noarch.rpm $@

## --

.PHONY: default clean package install test
