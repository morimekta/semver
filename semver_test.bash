#!/usr/bin/env bash
# shellcheck disable=SC2119

semver() {
  ./src/bin/semver.py "$@" 2>&1
}

HELP=$(cat <<EOF
usage: semver.py [-h] [--version] [--max] [--init | --range RNG]
                 [--increment LVL | --pre_release LVL] [--build BLV]
                 [--preid ID] [--verbose]
                 [v ...]
EOF
)

testHelp() {
  assertEquals "$HELP" "$(semver)"
}

testSimple() {
  assertEquals "1.0.0" "$(semver 1)"
  assertEquals "1.2.0" "$(semver 1.2)"
  assertEquals "1.2.3" "$(semver 1.2.3)"
  assertEquals "1.2.3-foo" "$(semver 1.2.3-foo)"
  assertEquals "1.2.3-foo+bar" "$(semver 1.2.3-foo+bar)"
  assertEquals "1.2.3+bar" "$(semver 1.2.3+bar)"
}

testIncrement_major() {
  assertEquals "2.0.0" "$(semver -i major 1)"
  assertEquals "1.0.0" "$(semver -i major 1-foo)"
  assertEquals "2.0.0" "$(semver -i major 1.2)"
  assertEquals "2.0.0" "$(semver -i major 1.2-foo)"
  assertEquals "2.0.0" "$(semver -i major 1.2.3)"
  assertEquals "2.0.0" "$(semver -i major 1.2.3-foo)"
  assertEquals "2.0.0" "$(semver -i major 1.2.3-foo+bar)"
  assertEquals "2.0.0" "$(semver -i major 1.2.3+bar)"
}

testIncrement_minor() {
  assertEquals "1.1.0" "$(semver -i minor 1)"
  assertEquals "1.0.0" "$(semver -i minor 1-foo)"
  assertEquals "1.3.0" "$(semver -i minor 1.2)"
  assertEquals "1.2.0" "$(semver -i minor 1.2-foo)"
  assertEquals "1.3.0" "$(semver -i minor 1.2.3)"
  assertEquals "1.3.0" "$(semver -i minor 1.2.3-foo)"
  assertEquals "1.3.0" "$(semver -i minor 1.2.3-foo+bar)"
  assertEquals "1.3.0" "$(semver -i minor 1.2.3+bar)"
}

testIncrement_patch() {
  assertEquals "1.0.1" "$(semver -i patch 1)"
  assertEquals "1.0.0" "$(semver -i patch 1-foo)"
  assertEquals "1.2.1" "$(semver -i patch 1.2)"
  assertEquals "1.2.0" "$(semver -i patch 1.2-foo)"
  assertEquals "1.2.4" "$(semver -i patch 1.2.3)"
  assertEquals "1.2.3" "$(semver -i patch 1.2.3-foo)"
  assertEquals "1.2.3" "$(semver -i patch 1.2.3-foo+bar)"
  assertEquals "1.2.4" "$(semver -i patch 1.2.3+bar)"
}

testIncrementPR_major() {
  assertEquals "2.0.0-1"     "$(semver -p major 1)"
  assertEquals "1.0.0-foo.1" "$(semver -p major 1-foo)"
  assertEquals "2.0.0-1"     "$(semver -p major 1.2)"
  assertEquals "1.2.0-foo.1" "$(semver -p major 1.2-foo)"
  assertEquals "2.0.0-1"     "$(semver -p major 1.2.3)"
  assertEquals "1.2.3-foo.1" "$(semver -p major 1.2.3-foo)"
  assertEquals "1.2.3-foo.1" "$(semver -p major 1.2.3-foo+bar)"
  assertEquals "2.0.0-1"     "$(semver -p major 1.2.3+bar)"
}

testIncrementPR_minor() {
  assertEquals "1.1.0-1"     "$(semver -p minor 1)"
  assertEquals "1.0.0-foo.1" "$(semver -p minor 1-foo)"
  assertEquals "1.3.0-1"     "$(semver -p minor 1.2)"
  assertEquals "1.2.0-foo.1" "$(semver -p minor 1.2-foo)"
  assertEquals "1.3.0-1"     "$(semver -p minor 1.2.3)"
  assertEquals "1.2.3-foo.1" "$(semver -p minor 1.2.3-foo)"
  assertEquals "1.2.3-foo.1" "$(semver -p minor 1.2.3-foo+bar)"
  assertEquals "1.3.0-1"     "$(semver -p minor 1.2.3+bar)"
}

testIncrementPR_patch() {
  assertEquals "1.0.1-1"     "$(semver -p patch 1)"
  assertEquals "1.0.0-foo.1" "$(semver -p patch 1-foo)"
  assertEquals "1.2.1-1"     "$(semver -p patch 1.2)"
  assertEquals "1.2.0-foo.1" "$(semver -p patch 1.2-foo)"
  assertEquals "1.2.4-1"     "$(semver -p patch 1.2.3)"
  assertEquals "1.2.3-foo.1" "$(semver -p patch 1.2.3-foo)"
  assertEquals "1.2.3-foo.1" "$(semver -p patch 1.2.3-foo+bar)"
  assertEquals "1.2.4-1"     "$(semver -p patch 1.2.3+bar)"
}