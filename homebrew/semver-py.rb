class SemverPy < Formula
    desc "SemverPy"
    homepage "http://gitlab.com/morimekta/semver"
    version "__VERSION__"
    url "https://dl.morimekta.net/archive/semver-py-#{version}.tar.gz"
    sha256 "__SHA256SUM__"

    # depends_on :python => "3.8+"

    def install
        bin.install Dir["bin/*"]
    end
end
