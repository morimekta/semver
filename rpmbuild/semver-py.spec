Summary: Simple command line tool to manage and parse semantic version strings
Name: semver-py
Version: __VERSION__
Release: 1
License: Apache-2.0
URL: https://gitlab.com/morimekta/semver
Group: Development
Packager: Stein Eldar Johnsen
Requires: python3
BuildRoot: __ROOT__/out-rpm/out

%description
Simple command line tool to manage and parse semantic version strings.

%files
%attr(0744, root, root) /usr/local/bin/*

%prep
