Semantic Version Script
=======================

[![Morimekta](https://img.shields.io/static/v1?label=morimekta.net&message=semver&color=informational)](https://morimekta.net/semver/)
[![License](https://img.shields.io/static/v1?label=license&message=apache%202.0&color=informational)](https://apache.org/licenses/LICENSE-2.0)

Simple python script that follows semantic versioning and can do 
proper incrementation and annotation of updated versions. 

- [SemVer 2.0.0](https://semver.org/)



```
$ semver --help
usage: semver [-h] [--version] [--max] [--init | --range RNG]
              [--increment LVL | --pre_release LVL] [--build BLV] [--preid ID] [--verbose]
              [v ...]

Update semantic versions, and return them in sorted order.

positional arguments:
  v                     Version to increment.

options:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  --max, -M             Only return the highest version. This will also skip all unparseable
                        versions from input.
  --init                Initialize a 'zero' version if none is provided or found (see --range).
  --range RNG, -r RNG   Range match criteria to be satisfied with each version. Filter is applied
                        before maxed or incremented.
  --increment LVL, -i LVL
                        Increment version, one of [major, minor, patch, pre-major, pre-minor,
                        pre-patch].
  --pre_release LVL, -p LVL
                        Pre-release increment version, one of [major, minor, patch]. Same as '-i
                        pre-${LVL}'
  --build BLV, -B BLV   Build info, only set if incremented.
  --preid ID, -P ID     Pre-release prefix, only set if incremented with pre-*. Will be use as
                        prefix for the pre-release version, e.g. 'alpha' -> 'alpha.1'
  --verbose, -V         Show verbose error output.

Simple Version level increments:
 major           1.*          -> 2.0.0
 minor           1.2.*        -> 1.3.0
 patch           1.2.3(-*)?   -> 1.2.4

Version syntax:
 1.2.3-4+5
 {major}.{minor}.{patch}(-{pre-release})?(+{build-info})?

Increment version based on the increment level and current version.

major: increment major version, clear all other
minor: increment minor version, keep major, clear others
patch: increment patch version, keep major, minor, clear pre-release
pre-patch:
 - if already pre-release: Try to increment last number in pre-release parts. Or add '.1'
 - else increment patch and set pre-release to '1'
pre-minor:
 - if already pre-release: Try to increment last number in pre-release parts. Or add '.1'
 - else increment minor, set patch to 0 and set pre-release to '1'
pre-major:
 - if already pre-release: Try to increment last number in pre-release parts. Or add '.1'
 - else increment major, set minor and patch to 0 and set pre-release to '1'

Note that pre-* increments ignore any and all core version increment
if already a pre-release version.

Build info is always replaced on increments.

Filters:

 -r '[4..5)'                      matches (>= 4.0.0) (<5.0.0)
 -r '> 0.1'                       matches (>0.1.0
 -p ''

Examples:
 semver                           displays simplified help
 semver -h                        displays this
 semver --init                    returns 0.0.0
 semver 3.1 3.0.1                 returns 3.0.1 3.1
 semver 3.1 3.0.1 --max           returns 3.1

 semver -i pre-patch  3.0.8       returns 3.0.9-1
 semver -i pre-minor v3.0.8       returns 3.1.0-1
 semver -i pre-major  3.0.8       returns 4.0.0-1
 semver -i patch     v3.0.8       returns 3.0.9
 semver -i minor      3.0.8       returns 3.1.0
 semver -i major     v3.0.8       returns 4.0.0

 semver -i pre-patch v3.0.8-1     returns 3.0.8-2
 semver -i pre-minor  3.0.8-1     returns 3.0.8-2
 semver -i pre-minor  3.1.0-1     returns 3.1.0-2
 semver -i pre-major v3.0.8-1     returns 3.0.8-2
 semver -i pre-major v4.0.0-1     returns 4.0.0-2
 semver -i patch      3.0.9-1     returns 3.0.9
 semver -i minor     v3.1.0-1     returns 3.1.0
 semver -i major      4.0.0-1     returns 4.0.0

```

## To build

```shell
# remember to check out repos:
# - dl.morimekta.net
# - homebrew-tools
# Not installed on debian/ubuntu per default.
sudo apt install -y rpm
# If not installed, these are also required.
sudo apt install -y make dpkg-dev fakeroot

make clean package
make upload

# go to dl.morimekta.net, build and upload.
# commit and push homebrew-tools
```