#!/usr/bin/env python3
#
# Simple command to parse and handle semantic versions.

import argparse
import re
from enum import Enum

import sys

__version__ = '0.2.1'

SEMVER = re.compile(r'^v?'
                    r'(?P<major>[0-9]+)'
                    r'(?:\.(?P<minor>[0-9]+)'
                    r'(?:\.(?P<patch>[0-9]+))?)?'
                    r'(?:-(?P<pre_release>[a-zA-Z0-9][-._a-zA-Z0-9]*))?'
                    r'(?:\+(?P<build>[a-zA-Z0-9][-._a-zA-Z0-9]*))?'
                    r'$')
NUMERIC = re.compile(r'^[0-9]+$')
SPACES = re.compile(r'[\s]+')


class Level(Enum):
    MAJOR = 'major'
    MINOR = 'minor'
    PATCH = 'patch'


def parse_level(name: str):
    if name == 'major':
        return Level.MAJOR
    if name == 'minor':
        return Level.MINOR
    if name == 'patch':
        return Level.PATCH
    raise Warning("Unknown level " + str(name))


def int_or_zero(val):
    if val is None:
        return 0
    return int(str(val))


def none_to_empty(val):
    if val is None:
        return ''
    return str(val)


def is_int(val):
    try:
        int(val)
        return True
    except ValueError:
        return False


def pre_release_cmp(pr1, pr2):
    """
    Sort based on per '.' separated parts. Handle two numbers as pure numeric comparison.

    Returns comparison of the two values.
    """
    if (pr1 is None) != (pr2 is None):
        if pr1 is None:
            return 1
        return -1
    if pr1 is None:
        return 0

    l1 = pr1.split('.')
    l2 = pr2.split('.')
    while len(l1) > 0 and len(l2) > 0:
        p1 = l1[0]
        p2 = l2[0]
        if NUMERIC.match(p1) and NUMERIC.match(p2):
            n1 = int(p1)
            n2 = int(p2)
            if n1 != n2:
                return n1 - n2
        elif NUMERIC.match(p1):
            return -1
        elif NUMERIC.match(p2):
            return 1
        elif p1 < p2:
            return -1
        elif p1 > p2:
            return 1
        l1.pop(0)
        l2.pop(0)
    return len(l1) - len(l2)


def pre_release_lt(pr1, pr2):
    return pre_release_cmp(pr1, pr2) < 0


class SemVer(object):
    def __init__(self,
                 version: str,
                 major: int = None,
                 minor: int = None,
                 patch: int = None,
                 pre_release: str = None,
                 build_info: str = None):
        if version is not None:
            self.__str = str(version).removeprefix("v")
        else:
            self.__str = None

        if major is not None:
            self.__major = int_or_zero(major)
            self.__minor = int_or_zero(minor)
            self.__patch = int_or_zero(patch)
            self.__pre_release = pre_release
            self.__build = build_info
        else:
            v = SEMVER.search(str(version))
            if v is None:
                raise Warning('invalid version \'%s\'' % version)
            self.__major = int_or_zero(v.group('major'))
            self.__minor = int_or_zero(v.group('minor'))
            self.__patch = int_or_zero(v.group('patch'))
            self.__pre_release = v.group('pre_release')
            self.__build = v.group('build')

    def is_pre_release(self):
        return self.__pre_release is not None

    def with_pre_release(self, pre_release: str = None):
        if self.__pre_release is not pre_release:
            self.__str = None
        self.__pre_release = pre_release
        return self

    def with_build_info(self, build_info: str = None):
        if self.__build is not build_info:
            self.__str = None
        self.__build = build_info
        return self

    def increment(self, level: Level):
        self.__str = None
        if self.__pre_release is not None:
            self.__pre_release = None
            if (level == Level.MINOR and self.__patch > 0) or\
               (level == Level.MAJOR and (self.__patch > 0 or self.__minor > 0)):
                self.increment(level)
            return self
        elif level == Level.MAJOR:
            self.__major = self.__major + 1
            self.__minor = 0
            self.__patch = 0
        elif level == Level.MINOR:
            if self.__pre_release is None or self.__patch != 0:
                self.__minor = self.__minor + 1
            self.__patch = 0
        elif level == Level.PATCH:
            if self.__pre_release is None:
                self.__patch = self.__patch + 1
        else:
            raise Warning('Invalid increment level: ' + str(level))
        self.__pre_release = None
        return self

    def increment_pre_release(self, level: Level, pre_id: str = None):
        self.__str = None
        if self.__pre_release is not None:
            self.__increment_pre_release(pre_id)
        else:
            self.increment(level)
            if pre_id is not None:
                self.__pre_release = pre_id + '.1'
            else:
                self.__pre_release = '1'
        return self

    def __increment_pre_release(self, preid: str = None):
        pr1 = self.__pre_release.split('.')
        if is_int(pr1[-1]):
            prefix = '.'.join(pr1[:-1])
            if preid is not None:
                if preid == prefix:
                    self.__pre_release = preid + '.' + str(int(pr1[-1]) + 1)
                else:
                    self.__pre_release = preid + '.1'
            else:
                if prefix != '':
                    self.__pre_release = prefix + '.' + str(int(pr1[-1]) + 1)
                else:
                    self.__pre_release = str(int(pr1[-1]) + 1)
        else:
            self.__pre_release = self.__pre_release + '.1'

    def __cmp__(self, other):
        if self.__major != other.__major:
            return self.__major - other.__major
        if self.__minor != other.__minor:
            return self.__minor - other.__minor
        if self.__patch != other.__patch:
            return self.__patch - other.__patch
        return pre_release_cmp(self.__pre_release, other.__pre_release)

    def __eq__(self, other):
        return self.__cmp__(other) == 0

    def __lt__(self, other):
        return self.__cmp__(other) < 0

    def __le__(self, other):
        return self.__cmp__(other) <= 0

    def __str__(self):
        if self.__str is not None:
            return self.__str
        out = '%d.%d.%d' % (self.__major, self.__minor, self.__patch)
        if self.__pre_release is not None:
            out += '-%s' % self.__pre_release
        if self.__build is not None:
            out += '+%s' % self.__build
        return out


NULL_VER = SemVer("", major=0, minor=0, patch=0, build_info='')
MAX_VER = SemVer("", major=sys.maxsize)


class Matcher(object):
    def __init__(self):
        pass

    def match(self, semver: SemVer):
        """
        :return: True if matching.
        """
        return False


class RangeMatcher(Matcher):
    def __init__(self,
                 min_version: SemVer,
                 min_inclusive: bool,
                 max_version: SemVer,
                 max_inclusive: bool,
                 allow_pre_release: bool = True):
        Matcher.__init__(self)
        self.__min_version = min_version
        self.__min_inclusive = min_inclusive
        self.__max_version = max_version
        self.__max_inclusive = max_inclusive
        self.__allow_pre_release = allow_pre_release

    def match(self, semver):
        if semver.is_pre_release() and not self.__allow_pre_release:
            return False

        if self.__min_inclusive:
            if self.__min_version > semver:
                return False
        else:
            if self.__min_version >= semver:
                return False
        if self.__max_inclusive:
            if self.__max_version < semver:
                return False
        else:
            if self.__max_version <= semver:
                return False
        return True


class PlusMatcher(Matcher):
    def __init__(self, min_spec: SemVer, level: Level):
        Matcher.__init__(self)
        self.__min_spec = min_spec
        self.__level = level


SEMVER_CORE = r'[0-9]+(?:[.](?:[0-9]+)(?:[.](?:[0-9]+))?)?'
SPEC_MAVEN = re.compile(r'^(?P<pre>[(\[])'
                        r'(?P<min>' + SEMVER_CORE + ')?,'
                        r'(?P<max>' + SEMVER_CORE + ')?'
                        r'(?P<post>[)\]])$')
SPEC_COMPARE = re.compile(r'^(?P<op>(=|<|<=|>|>=))\s*(?P<version>' + SEMVER_CORE + ')$')
SPEC_PLUS = re.compile(r'^(?P<major>[0-9]+)(\.(?P<minor>[0-9]+)(\.(?P<patch>[0-9]+))?)?\+$')


def matcher(spec: str):
    spec = str(spec)

    s = SPEC_MAVEN.search(spec)
    if s is not None:
        min_v = NULL_VER
        max_v = MAX_VER
        v = s.group('min')
        if v is not None:
            min_v = SemVer(v)
        v = s.group('max')
        if v is not None:
            max_v = SemVer(v)
        if min_v == NULL_VER and max_v == MAX_VER:
            raise Warning("No version limiting range")
        if min_v > max_v:
            raise Warning(s.group('min') + " > " + s.group('max'))
        if min_v == max_v:
            raise Warning(s.group('min') + " == " + s.group('max'))
        return RangeMatcher(min_v, '[' == s.group('pre'),
                            max_v, ']' == s.group('post'))

    s = SPEC_COMPARE.search(spec)
    if s is not None:
        v = SemVer(s.group('version'))
        op = s.group('op')
        if op == '=':
            return RangeMatcher(v, True, v, True)
        if op == '<':
            return RangeMatcher(NULL_VER, True, v, False)
        if op == '<=':
            return RangeMatcher(NULL_VER, True, v, True)
        if op == '>':
            return RangeMatcher(v, False, MAX_VER, True)
        if op == '>=':
            return RangeMatcher(v, True, MAX_VER, True)
        raise Warning('Invalid version operation: ' + op)

    s = SPEC_PLUS.search(spec)
    if s is not None:
        if s.group('patch') is not None:
            return PlusMatcher(SemVer("",
                                      int_or_zero(s.group('major')),
                                      int_or_zero(s.group('minor')),
                                      int_or_zero(s.group('patch'))),
                               Level.PATCH)
        if s.group('minor') is not None:
            return PlusMatcher(SemVer("",
                                      int_or_zero(s.group('major')),
                                      int_or_zero(s.group('minor'))),
                               Level.MINOR)
        return PlusMatcher(SemVer("", int_or_zero(s.group('major'))),
                           Level.MAJOR)

    raise Warning("No matcher for spec: '" + spec + "'")


def main(argv):
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description='Update semantic versions, and return them in sorted order.',
        epilog='''
Simple Version level increments:
 major           1.*          -> 2.0.0
 minor           1.2.*        -> 1.3.0
 patch           1.2.3(-*)?   -> 1.2.4

Version syntax:
 1.2.3-4+5
 {major}.{minor}.{patch}(-{pre-release})?(+{build-info})?

Increment version based on the increment level and current version.

major: increment major version, clear all other
minor: increment minor version, keep major, clear others
patch: increment patch version, keep major, minor, clear pre-release
pre-patch:
 - if already pre-release: Try to increment last number in pre-release parts. Or add '.1'
 - else increment patch and set pre-release to '1'
pre-minor:
 - if already pre-release: Try to increment last number in pre-release parts. Or add '.1'
 - else increment minor, set patch to 0 and set pre-release to '1'
pre-major:
 - if already pre-release: Try to increment last number in pre-release parts. Or add '.1'
 - else increment major, set minor and patch to 0 and set pre-release to '1'

Note that pre-* increments ignore any and all core version increment
if already a pre-release version.

Build info is always replaced on increments.

Filters:

 -r '[4..5)'                      matches (>= 4.0.0) (<5.0.0)
 -r '> 0.1'                       matches (>0.1.0
 -p ''

Examples:
 semver                           displays simplified help
 semver -h                        displays this
 semver --init                    returns 0.0.0
 semver 3.1 3.0.1                 returns 3.0.1 3.1
 semver 3.1 3.0.1 --max           returns 3.1

 semver -i pre-patch  3.0.8       returns 3.0.9-1
 semver -i pre-minor v3.0.8       returns 3.1.0-1
 semver -i pre-major  3.0.8       returns 4.0.0-1
 semver -i patch     v3.0.8       returns 3.0.9
 semver -i minor      3.0.8       returns 3.1.0
 semver -i major     v3.0.8       returns 4.0.0

 semver -i pre-patch v3.0.8-1     returns 3.0.8-2
 semver -i pre-minor  3.0.8-1     returns 3.0.8-2
 semver -i pre-minor  3.1.0-1     returns 3.1.0-2
 semver -i pre-major v3.0.8-1     returns 3.0.8-2
 semver -i pre-major v4.0.0-1     returns 4.0.0-2
 semver -i patch      3.0.9-1     returns 3.0.9
 semver -i minor     v3.1.0-1     returns 3.1.0
 semver -i major      4.0.0-1     returns 4.0.0
''')
    parser.add_argument('--version', action='version', version='%(prog)s ' + __version__)
    parser.add_argument('--max', '-M', action='store_true',
                        help='Only return the highest version. This will also skip all unparseable versions from '
                             'input.')

    p_group = parser.add_mutually_exclusive_group()
    p_group.add_argument('--init', action='store_true',
                         help='Initialize a \'zero\' version if none is provided or found (see --range).')
    p_group.add_argument('--range', '-r', metavar='RNG', type=str,
                         help='Range match criteria to be satisfied with each version. '
                              'Filter is applied before maxed or incremented.')

    i_group = parser.add_mutually_exclusive_group()
    i_group.add_argument('--increment', '-i', metavar='LVL', type=str,
                         choices=['major', 'minor', 'patch', 'pre-major', 'pre-minor', 'pre-patch'],
                         help='Increment version, one of [major, minor, patch, pre-major, pre-minor, pre-patch].')
    i_group.add_argument('--pre_release', '-p', metavar='LVL', type=str,
                         choices=['major', 'minor', 'patch'],
                         help='Pre-release increment version, one of [major, minor, patch]. Same as \'-i pre-${LVL}\'')

    parser.add_argument('--build', '-B', metavar='BLV', type=str,
                        help='Build info, only set if incremented.')
    parser.add_argument('--preid', '-P', metavar='ID', type=str,
                        help='Pre-release prefix, only set if incremented with pre-*. Will be use as prefix for the '
                             'pre-release version, e.g. \'alpha\' -> \'alpha.1\'')
    parser.add_argument('--verbose', '-V', action='store_true',
                        help='Show verbose error output.')
    parser.add_argument('v', nargs='*', type=str,
                        help='Version to increment.')
    args = parser.parse_args(argv[1:])

    try:
        filter_range = args.range is not None
        out = list()
        if len(args.v) == 0 and not sys.stdin.isatty():
            for line in sys.stdin.readlines():
                for word in SPACES.split(line):
                    if word.strip() != '':
                        try:
                            out.append(SemVer(word.strip()))
                        except Warning as w:
                            if args.max or args.init:
                                continue
                            raise w
            sys.stdin.close()
        else:
            for v in args.v:
                try:
                    out.append(SemVer(v.strip()))
                except Warning as w:
                    if args.max or args.init or filter_range:
                        continue
                    raise w

        if len(out) == 0:
            if args.init:
                out.append(SemVer('v0.0.0'))
            else:
                parser.print_usage(file=sys.stderr)
                sys.exit(1)
        elif filter_range:
            m = matcher(args.range)
            out = list(filter(lambda x: m.match(x), out))
            if len(out) == 0:
                raise Warning('No versions left after filtering.')

        out.sort()
        if args.max:
            out = [out[-1]]

        if args.increment is not None:
            if len(out) > 1:
                raise Warning('Only one version can be provided when incrementing, got %i.' % len(out))
            if args.increment[:4] == 'pre-':
                inc = parse_level(args.increment[4:])
                for v in out:
                    v.increment_pre_release(inc, args.preid) \
                        .with_build_info(args.build)
            else:
                inc = parse_level(args.increment)
                for v in out:
                    v.increment(inc)\
                        .with_build_info(args.build)
        if args.pre_release is not None:
            if len(out) > 1:
                raise Warning('Only one version can be provided when incrementing, got %i.' % len(out))
            for v in out:
                v.increment_pre_release(parse_level(args.pre_release), args.preid)\
                    .with_build_info(args.build)
        for v in out:
            print(v)
    except Warning as w:
        if args.verbose:
            raise w
        print("Error:", w, file=sys.stderr)
        sys.exit(1)


if __name__ == '__main__':
    try:
        main(sys.argv)
    except KeyboardInterrupt as i:
        sys.exit(1)
